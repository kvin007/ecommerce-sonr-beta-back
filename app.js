
const express   = require('express'),
    app         = express(),
    bodyParser  = require('body-parser'),
    mongoose    = require('mongoose'),
    cors        = require('cors'),
    passport    = require('passport'),
    router = express.Router();


//routes
var productRoutes = require("./routes/products"),
    categoryRoutes = require("./routes/categories"),
    userRoutes = require("./routes/users"),
    indexRoutes = require("./routes/index");


app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

mongoose.connect("mongodb://localhost/ecommerce");

const connection = mongoose.connection;
connection.once('open', () => {
    console.log("MongoDB database connection established");
});

app.use(indexRoutes);
app.use("/products",productRoutes);
app.use("/categories",categoryRoutes);
app.use("/users",userRoutes);

app.listen(8010,'localhost',function(){
    console.log("Server has started");
});