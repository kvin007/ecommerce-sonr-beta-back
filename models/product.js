var mongoose = require('mongoose');

var productSchema = new mongoose.Schema({
    name: String,
    price: Number,
    image: String,
    description: String,
    category_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Category"
    },
    category_name: String,
});

module.exports = mongoose.model("Product", productSchema);
