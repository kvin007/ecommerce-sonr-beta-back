
var express = require('express');
var router = express.Router();
var Product = require("../models/product");

//Show all products
router.get("/",function(req,res){
    Product.find({},function(err,allProducts){
        if (err) console.log(err);
        else {
            res.json(allProducts);
        }
    });
});

//Shows info about a particular product
router.get("/:id",function(req,res){
    Product.findById(req.params.id,function(err,foundProduct){
        if (err) console.log(err);
        else{
            res.json(foundProduct);
        }
    });
});

//Create a new product
router.post("/",function(req,res){
    var name = req.body.name;
    var price = req.body.price;
    var image = req.body.image;
    var desc = req.body.description;
    var category_id = req.body.category_id;
    var category_name = req.body.category_name;
    
    var newProduct = {name:name,price:price,image:image,description:desc,category_id:category_id,category_name:category_name}
    Product.create(newProduct,function(err,newlyCreated){
        if (err) res.status(400).send('Failed to create new product');
        else res.status(200).json(newlyCreated);
    });
});


//Update certain product
router.put("/:id",function(req,res){
    Product.findByIdAndUpdate(req.params.id,req.body,function(err,updatedProduct){
        if (err) return res.status(500).send(err.message);
        res.status(200).json(updatedProduct);
    })
});

//Destroy certain product
router.delete("/:id",function(req,res){
    Product.findByIdAndRemove(req.params.id,function(err){
        if (err) return res.status(500).send(err.message);
        res.status(200).send();
    })
})


module.exports = router;