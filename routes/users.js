
var express = require('express');
var router = express.Router();
var User = require("../models/user");

//Show all users
router.get("/",function(req,res){
    User.find({},function(err,allUsers){
        if (err) console.log(err);
        else {
            res.json(allUsers);
        }
    });
});

//Shows info about a particular user
router.get("/:id",function(req,res){
    User.findById(req.params.id,function(err,foundUser){
        if (err) console.log(err);
        else{
            res.json(foundUser);
        }
    });
});



//Create a new user
router.post("/",function(req,res){
    var username = req.body.username;
    var password = req.body.password;
    var newUser = {username:username,password:password}
    User.create(newUser,function(err,newlyCreated){
        if (err) res.status(400).send('Failed to create new user');
        else res.status(200).json(newlyCreated);
    });
});


//Find
router.post("/find",function(req,res){
    var username = req.body.username;
    var password = req.body.password;
    var newUser = {username:username,password:password}
    
    User.find(newUser,function(err,foundUser){
        console.log(foundUser.length);
        if (err) res.send({permission: false});
        else if (foundUser.length===0) res.json({permission: false});
        else res.json({permission: true});
    });
});

//Update certain user
router.put("/:id",function(req,res){
    User.findByIdAndUpdate(req.params.id,req.body,function(err,updateUser){
        if (err) return res.status(500).send(err.message);
        res.status(200).json(updateUser);
    })
});

//Destroy certain user
router.delete("/:id",function(req,res){
    User.findByIdAndRemove(req.params.id,function(err){
        if (err) return res.status(500).send(err.message);
        res.status(200).send();
    })
});

module.exports = router;