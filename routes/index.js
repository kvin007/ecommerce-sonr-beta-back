
var express = require('express');
var router = express.Router();
var passport = require('passport');
var Product = require("../models/product");

//Show all products
router.get("/",function(req,res){
    Product.find({},function(err,allProducts){
        if (err) console.log(err);
        else {
            res.json(allProducts);
        }
    });
});


module.exports = router;
