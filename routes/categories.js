
var express = require('express');
var router = express.Router();
var Category = require("../models/category");

//Show all categories
router.get("/",function(req,res){
    Category.find({},function(err,allCategories){
        if (err) console.log(err);
        else {
            res.json(allCategories);
        }
    });
});

//Shows info about a particular category
router.get("/:id",function(req,res){
    Category.findById(req.params.id,function(err,foundCategory){
        if (err) console.log(err);
        else{
            res.json(foundCategory);
        }
    });
});

//Create a new category
router.post("/",function(req,res){
    var name = req.body.name;
    var desc = req.body.description;
    var newCategory = {name:name,description:desc}
    Category.create(newCategory,function(err,newlyCreated){
        if (err) res.status(400).send('Failed to create new category');
        else res.status(200).json(newlyCreated);
    });
});

//Update certain category
router.put("/:id",function(req,res){
    Category.findByIdAndUpdate(req.params.id,req.body,function(err,updateCategory){
        if (err) return res.status(500).send(err.message);
        res.status(200).json(updateCategory);
    })
});

//Destroy certain category
router.delete("/:id",function(req,res){
    Category.findByIdAndRemove(req.params.id,function(err){
        if (err) return res.status(500).send(err.message);
        res.status(200).send();
    })
});

module.exports = router;